//
//  Calculate.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-19.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

// Touple that holds the two operational values
typealias parameter = (Double, Double)

protocol Calculate {
    // Function to add two number values
    func add(_ parameter: parameter) -> String
    // Function to substract second number from first number
    func subtract(_ parameter: parameter) -> String
    // Function to divide first number by second number
    func divide(_ parameter: parameter) -> String
    // Function to multiply two number values
    func multiply(_ parameter: parameter) -> String
}

extension Calculate {
    
    func add(_ parameter: parameter) -> String {
        return (parameter.0 + parameter.1).clean
    }
    
    func subtract(_ parameter: parameter) -> String {
        return (parameter.0 - parameter.1).clean
    }
    
    func divide(_ parameter: parameter) -> String {
        return (parameter.0 / parameter.1).clean
    }
    
    func multiply(_ parameter: parameter) -> String {
        return (parameter.0 * parameter.1).clean
    }
}


