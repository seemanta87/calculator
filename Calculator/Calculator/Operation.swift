//
//  Operation.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-20.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import Foundation

enum Operation: Int {
    case reset = 11
    case add
    case subtract
    case multiply
    case divide
    case result
    
}

extension Operation: Calculate {
    func execute(param: parameter) -> String {
        switch self {
        case .add:
            return add(param)
        case .subtract:
            return subtract(param)
        case.multiply:
            return multiply(param)
        case .divide:
            return divide(param)
        default: break
            // No calculation is required for Result and Reset
        }
        return ""
    }
}
