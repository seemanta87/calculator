//
//  ViewController.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-19.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {
    
    // MARK: - Static Values

    static let keyboardButtonAccessibilityIdentifier = "keyboardButton"
    static let displayLabelAccessibilityIdentifier = "displayResultLabel"
    
    // MARK: - IBOutlets
    
    // Collection to holds all the keys of the calculator
    @IBOutlet var buttonCollection: [UIButton]!
    // Label to display the result
    @IBOutlet weak var resultDisplayLabel: UILabel!
    
    // MARK: - Private Variables
    private var resultValue: Double = 0.0
    
    private var selectedOperation: Operation? {
        didSet {
            stylizeButton()
        }
    }
    
    private var operationSelected: Bool = false {
        didSet {
            guard operationSelected else { return }
            guard
                let displayValue = resultDisplayLabel.text,
                let newValue = Double(displayValue)
            else { return }
            resultValue = newValue
        }
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        stylizeButton()
        setIdentifiersForTesting()
        resultDisplayLabel.accessibilityIdentifier = ViewController.displayLabelAccessibilityIdentifier
    }

    // MARK: - IBAction
    @IBAction func didSelectNumber(_ sender: UIButton) {
        if operationSelected {
            clean()
        }
        guard let existingValue = resultDisplayLabel.text  else { return }
        resultDisplayLabel.text = (existingValue + String(sender.tag)).clean
    }
    
    @IBAction func didSelectAction(_ sender: UIButton) {
        
        guard let operatorValue = Operation(rawValue: sender.tag) else { return }
        
        switch operatorValue {
            
        case .reset:
            resetAll()
            
        case .result:
            // Execute the last selected operation
            executeLastSelectedOperation()
            selectedOperation = nil
            
        default:
            // Frist try to Execute the last selected operation
            executeLastSelectedOperation()
            // Save operation type for the calculation
            selectedOperation = operatorValue
        }
        operationSelected = true
    }
    
    // MARK: - Private Methods

    private func executeLastSelectedOperation() {
        guard
            let displayValue = resultDisplayLabel.text,
            let newValue = Double(displayValue),
            let operation = selectedOperation
            else { return }
        clean()
        resultDisplayLabel.text = operation.execute(param: (resultValue, newValue))
    }
    
    private func stylizeButton() {
        for button in buttonCollection {
            button.tag == selectedOperation?.rawValue ? button.setHighlighted() : button.setNormal()
        }
    }
    
    private func setIdentifiersForTesting() {
        for button in buttonCollection {
            button.accessibilityIdentifier = "\(ViewController.keyboardButtonAccessibilityIdentifier).\(button.tag)"
        }
    }
    
    // This function is used when user taps 'C'
    private func resetAll() {
        selectedOperation = nil
        clean()
    }
    
    // Clean function is used when any operation is tapped except reset or 'C'
    private func clean() {
        resultDisplayLabel.text = "0"
        operationSelected = false
    }
}

