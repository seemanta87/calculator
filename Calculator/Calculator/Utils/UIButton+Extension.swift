//
//  UIButton+Extension.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-20.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setNormal() {
        alpha = 1.0
        titleLabel?.font = UIFont.normal
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 0.5
    }
    
    func setHighlighted() {
        alpha = 0.8
        titleLabel?.font = UIFont.highlighted
    }
}

extension UIFont {
    static var normal: UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: 30)!
    }
    
    static var highlighted: UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: 30)!
    }
}
