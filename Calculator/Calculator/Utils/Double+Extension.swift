//
//  Double+Extension.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-20.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import Foundation

extension Double {
    // Remove a decimal from a float if the decimal is equal to 0
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
