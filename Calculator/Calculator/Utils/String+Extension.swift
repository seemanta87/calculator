//
//  String+Extension.swift
//  Calculator
//
//  Created by Seemanta Biswas on 2018-12-20.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import Foundation

extension String {
    // Remove or Ignore 0 from the begining of any numeric value
    var clean: String {
        guard let numberValue = Double(self) else { return "" }
        return numberValue.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", numberValue) : String(numberValue)
    }
}
