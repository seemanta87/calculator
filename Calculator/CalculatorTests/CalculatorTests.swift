//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Seemanta Biswas on 2018-12-19.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorTests: XCTestCase {

    func testKeyboardNormalFont_ExpectingHelveticaNeueLight() {
        let normalFont = UIFont.normal
        XCTAssertEqual(normalFont.fontName, "HelveticaNeue-Light")
        XCTAssertEqual(normalFont.pointSize, 30)
    }
    
    func testKeyboardHighlightedFont_ExpectingHelveticaNeueBold() {
        let normalFont = UIFont.highlighted
        XCTAssertEqual(normalFont.fontName, "HelveticaNeue-Bold")
        XCTAssertEqual(normalFont.pointSize, 30)
    }
    
    func testCalculatorFunction_ExpectSuccess() {
        let param = (20.0, 10.0)
        XCTAssertEqual(Operation.add.execute(param: param), "30")
        XCTAssertEqual(Operation.subtract.execute(param: param), "10")
        XCTAssertEqual(Operation.multiply.execute(param: param), "200")
        XCTAssertEqual(Operation.divide.execute(param: param), "2")
    }

}
