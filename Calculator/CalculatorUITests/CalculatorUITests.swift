//
//  CalculatorUITests.swift
//  CalculatorUITests
//
//  Created by Seemanta Biswas on 2018-12-19.
//  Copyright © 2018 Seemanta Biswas. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorUITests: XCTestCase {

    var app = XCUIApplication()
    var firstParameter: Int = 0
    var secondParameter: Int = 0
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        firstParameter = 0
        secondParameter = 0
    }

    // Addition Function test
    func testAdditionFunctionality_ExpectResult() {
        calculate(for: "keyboardButton.12")
        let actualresult = app.staticTexts["displayResultLabel"].label

        let result = Int(firstParameter) + Int(secondParameter)
        XCTAssertEqual(result, Int(actualresult))
    }
    
    // Subtraction Function test
    func testSubtractionFunctionality_ExpectResult() {
        calculate(for: "keyboardButton.13")
        let actualresult = app.staticTexts["displayResultLabel"].label
        
        let result = Int(firstParameter) - Int(secondParameter)
        XCTAssertEqual(result, Int(actualresult))
    }
    
    // Multiplication Function test
    func testMultiplicationFunctionality_ExpectResult() {
        calculate(for: "keyboardButton.14")
        let actualresult = app.staticTexts["displayResultLabel"].label
        
        let result = Int(firstParameter) * Int(secondParameter)
        XCTAssertEqual(result, Int(actualresult))
    }
    
    // Division Function test
    func testDivisionFunctionality_ExpectResult() {
        calculate(for: "keyboardButton.15")
        let actualresult = app.staticTexts["displayResultLabel"].label
        
        let result = Double(firstParameter) / Double(secondParameter)
        XCTAssertEqual(result, Double(actualresult))
    }
    
    func testResetFunctionality_ExpectEmpty() {
        typeRandomNumber()
        let operationButton = app.buttons["keyboardButton.11"]
        expect(exists: operationButton)
        operationButton.tap()
        
        XCTAssertEqual(app.staticTexts["displayResultLabel"].label, "0")
    }
    
    private func calculate(for operation: String) {
        typeRandomNumber()
        typeRandomNumber()
        firstParameter = Int(app.staticTexts["displayResultLabel"].label)!
        
        let operationButton = app.buttons[operation]
        expect(exists: operationButton)
        operationButton.tap()
        
        typeRandomNumber()
        typeRandomNumber()
        secondParameter = Int(app.staticTexts["displayResultLabel"].label)!
        
        let resultButton = app.buttons["keyboardButton.16"]
        expect(exists: resultButton)
        resultButton.tap()
    }
    
    // Type any random number between 1 - 10
    private func typeRandomNumber() {
        XCTContext.runActivity(named: "Type any random number between 1 - 10") { _ in
            let number = Int.random(in: 1 ..< 10)
            let button = app.buttons["keyboardButton.\(number)"]
            expect(exists: button)
            button.tap()
        }
    }
    
    /// Expect that the item exists
    ///
    /// - Parameters:
    ///   - element: The element to test for
    ///   - timeout: Optional timeout
    private func expect(exists element: XCUIElement, timeout: TimeInterval = 10) {
        let predicate = NSPredicate(format: "exists == true")
        let result = XCTWaiter().wait(for: [expectation(for: predicate, evaluatedWith: element, handler: nil)], timeout: timeout)
        if result != .completed {
            XCTFail("Failed while waiting for: \(element)")
        }
    }

}
